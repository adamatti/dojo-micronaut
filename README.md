#### Decisões de tecnologia
* [Micronaut](http://micronaut.io) - criado pelos criadores do groovy / gradle para microservices, similar ao spring boot porem mais rápido (por ser compilado) e usa menos memória
* [Groovy](http://groovy-lang.org/) - o mesmo java, mas menos verboso
* [Spock](http://spockframework.org/) - framework de teste do ecosistema do groovy, menos verboso, similar a DDD
* [Editorconfig](https://editorconfig.org/) - mesmo padrão de formatação para todo o time
* Mongo - ver [teorema cap](https://adamatti.github.io/blog/database/2017/07/16/teorema-cap.html)

#### Decisões de design
* Pauta: view -> repo: a camada de serviço não adicionaria valor nessa situação e não foi usada
* V1 - salvar em memória, v2 salvar no mongo
* Muitas melhoras podem ser aplicadas na parte reativa (rever quando bloquear)

#### Como rodar?
* `make deps` para iniciar docker / mongo
* `make test` para rodar os testes
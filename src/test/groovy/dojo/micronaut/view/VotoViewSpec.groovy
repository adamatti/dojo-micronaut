package dojo.micronaut.view

import dojo.micronaut.model.Pauta
import dojo.micronaut.model.SessaoDeVotacao
import dojo.micronaut.model.SimNaoEnum
import dojo.micronaut.model.Voto
import dojo.micronaut.repo.PautaRepo
import dojo.micronaut.repo.SessaoDeVotacaoRepo
import groovy.time.TimeCategory
import io.micronaut.context.ApplicationContext
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.runtime.server.EmbeddedServer
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class VotoViewSpec extends BaseSpec {
    def "voto - happy path"(){
        given:
            PautaRepo pautaRepo = embeddedServer.applicationContext.getBean(PautaRepo)
            Pauta pauta = pautaRepo.save(new Pauta(nome:"junit"))

            SessaoDeVotacaoRepo sessaoDeVotacaoRepo = embeddedServer.applicationContext.getBean(SessaoDeVotacaoRepo)
            SessaoDeVotacao sessao = sessaoDeVotacaoRepo.save(
                new SessaoDeVotacao(idPauta: pauta.id, dataExpiracao: tomorrow())
            )

            String idVotante = UUID.randomUUID().toString()
            Voto voto = new Voto(idSessao: sessao.id, voto: SimNaoEnum.SIM, idVotante: idVotante)
            HttpRequest request = HttpRequest.POST("/votos",voto)
        when: "Votar primeira vez"
            def response = client.toBlocking().exchange(request)
        then:
            response.status == HttpStatus.NO_CONTENT
        when: "Votar segunda vez"
            response = client.toBlocking().exchange(request)
        then:
            HttpClientResponseException e = thrown(HttpClientResponseException)
            e.status == HttpStatus.INTERNAL_SERVER_ERROR
    }

    private Date tomorrow(){
        Date result

        use(TimeCategory){
            result = new Date() + 1.day
        }

        result
    }
}

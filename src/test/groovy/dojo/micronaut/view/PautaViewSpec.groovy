package dojo.micronaut.view

import dojo.micronaut.model.Pauta
import io.micronaut.context.ApplicationContext
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class PautaViewSpec extends BaseSpec {
    def "criar uma pauta"(){
        given:
            Pauta pauta = new Pauta(nome: "junit")
            HttpRequest request = HttpRequest.POST("/pautas",pauta)
        when:
            def response = client.toBlocking().exchange(request)
        then:
            response.status == HttpStatus.NO_CONTENT
    }

}

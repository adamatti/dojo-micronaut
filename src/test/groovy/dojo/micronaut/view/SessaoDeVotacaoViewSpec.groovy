package dojo.micronaut.view

import dojo.micronaut.BusinessException
import dojo.micronaut.model.Pauta
import dojo.micronaut.model.SessaoDeVotacao
import dojo.micronaut.model.SimNaoEnum
import dojo.micronaut.model.Voto
import dojo.micronaut.repo.PautaRepo
import dojo.micronaut.repo.VotoRepo
import io.micronaut.context.ApplicationContext
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.runtime.server.EmbeddedServer
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

class SessaoDeVotacaoViewSpec extends BaseSpec {
    def "criar sessao com pauta"(){
        given:
            Pauta pauta = newPauta()
            String idSessao = UUID.randomUUID().toString()
            SessaoDeVotacao sessao = new SessaoDeVotacao(id: idSessao, idPauta: pauta.id)
            HttpRequest request = HttpRequest.POST("/sessoesVotacao",sessao)
        when: "Criar sessap"
            def response = client.toBlocking().exchange(request)
        then: "Sessao criada"
            response.status == HttpStatus.NO_CONTENT
        when: "Votar"
            Voto voto = new Voto(idVotante: "junit",voto: SimNaoEnum.SIM,idSessao: sessao.id)
            VotoRepo votoRepo = embeddedServer.applicationContext.getBean(VotoRepo)
            votoRepo.save(voto)

            request = HttpRequest.GET("/sessoesVotacao/${sessao.id}")
            Map responseMap = client.toBlocking().retrieve(request,Map)
        then:
            responseMap.idSessao == sessao.id
            responseMap.totalSim == 1
    }

    def "criar sessão sem pauta"(){
        given:
            SessaoDeVotacao sessao = new SessaoDeVotacao()
            HttpRequest request = HttpRequest.POST("/sessoesVotacao",sessao)
        when:
            def response = client.toBlocking().exchange(request)
        then:
            HttpClientResponseException e = thrown(HttpClientResponseException)
            e.status == HttpStatus.BAD_REQUEST
    }


    private Pauta newPauta(){
        PautaRepo pautaRepo = embeddedServer.applicationContext.getBean(PautaRepo)
        pautaRepo.save(new Pauta(nome: "junit"))
    }
}

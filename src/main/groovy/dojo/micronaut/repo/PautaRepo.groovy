package dojo.micronaut.repo

import com.mongodb.BasicDBObject
import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoCollection
import dojo.micronaut.model.Pauta
import groovy.transform.CompileStatic
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.bson.conversions.Bson

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@CompileStatic
class PautaRepo {
    @Inject
    private MongoClient mongoClient

    Pauta save(Pauta pauta){
        if (!pauta.id){
            pauta.id = UUID.randomUUID().toString()
        }

        Flowable.fromPublisher(
            getCollection().insertOne(pauta)
        ).blockingSubscribe()

        pauta
    }

    Maybe<Pauta> findById(String id){
        Bson bson = new BasicDBObject([_id: id])

        Flowable.fromPublisher(
            getCollection().find(bson).limit(1)
        ).firstElement()
    }

    private MongoCollection getCollection() {
        mongoClient
            .getDatabase("sample")
            .getCollection("pautas", Pauta.class)
    }
}

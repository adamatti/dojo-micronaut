package dojo.micronaut.repo

import com.mongodb.BasicDBObject
import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoCollection
import dojo.micronaut.model.SessaoDeVotacao
import dojo.micronaut.model.Voto
import groovy.transform.CompileStatic
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.bson.conversions.Bson

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@CompileStatic
class VotoRepo {
    @Inject
    private MongoClient mongoClient

    Voto save(Voto voto){
        if (!voto.id){
            voto.id = UUID.randomUUID().toString()
        }

        Flowable.fromPublisher(
            getCollection().insertOne(voto)
        ).blockingSubscribe()


        voto
    }

    Maybe<Voto> findByVotanteAndSession(String idVotante, String idSession){
        Bson bson = new BasicDBObject([idVotante:idVotante, idSessao: idSession])

        Flowable.fromPublisher(
            getCollection().find(bson)
        ).firstElement()

    }

    Single<List<Voto>> findBySession(String idSessao){
        Bson bson = new BasicDBObject([idSessao: idSessao])

        Flowable.fromPublisher(
            getCollection().find(bson)
        ).toList()
    }

    private MongoCollection<Voto> getCollection() {
        mongoClient
            .getDatabase("sample")
            .getCollection("votos", Voto.class)
    }
}

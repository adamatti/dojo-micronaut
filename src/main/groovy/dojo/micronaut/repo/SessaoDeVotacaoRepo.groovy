package dojo.micronaut.repo

import com.mongodb.BasicDBObject
import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoCollection
import dojo.micronaut.model.Pauta
import dojo.micronaut.model.SessaoDeVotacao
import groovy.transform.CompileStatic
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single
import org.bson.conversions.Bson

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@CompileStatic
class SessaoDeVotacaoRepo {
    @Inject
    private MongoClient mongoClient

    SessaoDeVotacao save(SessaoDeVotacao sessao){
        if (!sessao.id){
            sessao.id = UUID.randomUUID().toString()
        }

        Flowable.fromPublisher(
            getCollection().insertOne(sessao)
        ).blockingSubscribe()

        sessao
    }

    Maybe<SessaoDeVotacao> findById(String id){
        Bson bson = new BasicDBObject([_id: id])

        Flowable.fromPublisher(
            getCollection().find(bson).limit(1)
        ).firstElement()
    }

    private MongoCollection<SessaoDeVotacao> getCollection() {
        mongoClient
            .getDatabase("sample")
            .getCollection("sessoes", SessaoDeVotacao.class)
    }

}

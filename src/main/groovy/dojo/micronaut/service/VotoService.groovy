package dojo.micronaut.service

import dojo.micronaut.BusinessException
import dojo.micronaut.model.SessaoDeVotacao
import dojo.micronaut.model.Voto
import dojo.micronaut.repo.SessaoDeVotacaoRepo
import dojo.micronaut.repo.VotoRepo
import groovy.transform.CompileStatic

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@CompileStatic
class VotoService {
    @Inject
    private VotoRepo votoRepo

    @Inject
    private SessaoDeVotacaoRepo sessaoDeVotacaoRepo

    Voto votar(Voto voto){
        validaVoto(voto)
        votoRepo.save(voto)
    }

    private void validaVoto(Voto voto){
        if (!sessaoValida(voto.idSessao)) throw new BusinessException("Sessão Inválida")

        Voto entity = votoRepo.findByVotanteAndSession(voto.idVotante, voto.idSessao).blockingGet()
        if (entity){
            throw new BusinessException("Votante não pode votar 2x na mesma sessão")
        }
    }

    private boolean sessaoValida(String idSessao){
        if (!idSessao) return false
        SessaoDeVotacao sessao = sessaoDeVotacaoRepo.findById(idSessao).blockingGet()

        sessao.dataExpiracao > new Date()
    }
}

package dojo.micronaut.service

import dojo.micronaut.BusinessException
import dojo.micronaut.model.SessaoDeVotacao
import dojo.micronaut.repo.PautaRepo
import dojo.micronaut.repo.SessaoDeVotacaoRepo
import groovy.time.TimeCategory
import groovy.transform.CompileStatic

import javax.inject.Inject
import javax.inject.Singleton
import java.util.concurrent.TimeUnit

@Singleton
class SessaoDeVotacaoService {
    @Inject
    private PautaRepo pautaRepo

    @Inject
    private SessaoDeVotacaoRepo sessaoDeVotacaoRepo

    SessaoDeVotacao save(SessaoDeVotacao sessao){
        if (!pautaExistente(sessao.idPauta)){
            throw new BusinessException("Pauta inválida")
        }

        use( TimeCategory ) {
            sessao.dataExpiracao = sessao.dataExpiracao ?: new Date() + 1.minute
        }

        sessaoDeVotacaoRepo.save(sessao)
    }

    private boolean pautaExistente(String idPauta){
        idPauta && pautaRepo.findById(idPauta).blockingGet()
    }
}

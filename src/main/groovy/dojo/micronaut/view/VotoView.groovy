package dojo.micronaut.view

import dojo.micronaut.model.Voto
import dojo.micronaut.service.VotoService
import groovy.transform.CompileStatic
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import io.micronaut.validation.Validated

import javax.inject.Inject
import javax.inject.Singleton
import javax.validation.Valid

@Singleton
@CompileStatic
@Controller("/votos")
@Validated
class VotoView {
    @Inject
    private VotoService votoService

    @Post
    HttpResponse votar(@Body @Valid Voto voto){
        votoService.votar(voto)

        HttpResponse.noContent()
    }
}

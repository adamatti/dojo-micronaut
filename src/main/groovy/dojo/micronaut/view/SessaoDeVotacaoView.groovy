package dojo.micronaut.view

import dojo.micronaut.model.SessaoDeVotacao
import dojo.micronaut.model.SimNaoEnum
import dojo.micronaut.model.Voto
import dojo.micronaut.repo.SessaoDeVotacaoRepo
import dojo.micronaut.repo.VotoRepo
import dojo.micronaut.service.SessaoDeVotacaoService
import groovy.transform.CompileStatic
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Produces
import io.micronaut.validation.Validated

import javax.inject.Inject
import javax.validation.Valid
import javax.validation.constraints.NotNull

@Validated
@CompileStatic
@Controller("/sessoesVotacao")
class SessaoDeVotacaoView {
    @Inject
    private SessaoDeVotacaoRepo sessaoDeVotacaoRepo

    @Inject
    private SessaoDeVotacaoService sessaoDeVotacaoService

    @Inject
    private VotoRepo votoRepo

    @Post
    HttpResponse criarSessao(@Body @Valid SessaoDeVotacao sessao){
        sessaoDeVotacaoService.save(sessao)

        HttpResponse.noContent()
    }

    @Get("/{idSessao}")
    HttpResponse<Map> verSessao(@NotNull String idSessao){
        SessaoDeVotacao sessaoDeVotacao = sessaoDeVotacaoRepo.findById(idSessao).blockingGet()

        if (!sessaoDeVotacao){
            return HttpResponse.notFound()
        }

        List<Voto> votos = votoRepo.findBySession(idSessao).blockingGet()

        HttpResponse.ok([
            idSessao: idSessao,
            totalSim: votos.findAll{Voto it -> it.voto == SimNaoEnum.SIM }.size(),
            totalNao: votos.findAll{Voto it -> it.voto == SimNaoEnum.NAO }.size()
        ] as Map)
    }
}

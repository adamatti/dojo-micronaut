package dojo.micronaut.view

import dojo.micronaut.model.Pauta
import dojo.micronaut.repo.PautaRepo
import groovy.transform.CompileStatic
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import io.micronaut.validation.Validated

import javax.inject.Inject
import javax.validation.Valid

@Validated
@CompileStatic
@Controller("/pautas")
class PautaView {
    @Inject
    private PautaRepo pautaRepo

    @Post
    HttpResponse createPauta(@Body @Valid Pauta pauta){
        pautaRepo.save(pauta)

        HttpResponse.noContent()
    }
}

package dojo.micronaut.model

import groovy.transform.CompileStatic

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@CompileStatic
class Pauta {
    String id
    @NotBlank String nome
}

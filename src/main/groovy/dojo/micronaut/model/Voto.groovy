package dojo.micronaut.model

import groovy.transform.CompileStatic

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@CompileStatic
class Voto {
    String id
    @NotBlank String idVotante
    @NotBlank String idSessao
    @NotNull SimNaoEnum voto
}

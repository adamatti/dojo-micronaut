package dojo.micronaut

import groovy.transform.CompileStatic

@CompileStatic
class BusinessException extends Exception{
    BusinessException(String message){
        super(message)
    }
}

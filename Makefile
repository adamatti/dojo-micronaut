.DEFAULT_GOAL := help

deps: ## start docker dependencies
	docker-compose up -d

test: ## run tests
	./gradlew check

.PHONY: help
help: ## show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'